# ROBOT - Matrix Area

<p align="center"><img src="Matrix_Area.png" alt="ROBOT_Matrix_Area" width="300" height="250"/></p>

## About

The ROBOT is a simple application written in Python programming language <br />
in which the user gives an Integer value for the size of Matrix Area. <br />
The program tells the user the value he gave and instructs him <br />
appropriately so that he can move the robot in that area.<br />
The program ends as soon as the user gives the command "EXIT" from the keyboard. <br />
If the user gives a value that is outside of the limits of Matrix Area, <br />
it notifies him appropriately, <br />
about both the position of the robot and the possibilities in moves he has.

ROBOT was created with the following programming languages:

- Python


## License
ROBOT is free to use with open source license.

## Project status
ROBOT is up and running.
