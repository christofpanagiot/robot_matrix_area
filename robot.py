#ROBOT Program
import re
matrix = 0 #Matrix Area
x, y = 0, 0 #X and Y Coordinates
robot = 0 #ROBOT Position

#Loop for Checking the Matrix Area
while True :
    matrix = eval(input("Give a number for the size of Matrix Area: ")) #Matrix Area Size Given by User
    if matrix < 0 :
        print("Matrix Area is lower than 0 !")
    elif matrix == 1 :
        print("ROBOT cannot move, please give a higher number")
    elif isinstance(matrix,float) :
        print("Matrix Area can't be type of 3.14 (π), please give an Integer number")
    else :
        break

matrix = int(matrix) #Convert Matrix Variable to Int

#Loop for the ROBOT position in the Matrix Area
while True :
    print("Matrix Area is: ", matrix, " * ", matrix) #Matrix Area Size
    robot = input("Give a position for the robot Up (U), Down (D), Left (L), Right (R) and the number, i.e. R5, type EXIT for exit: ").upper() #ROBOT Position by User
    robot = re.split(r'(\d+)', robot)

    #Check the position of ROBOT in Matrix Area
    if robot[0] == "U" :
        y = y - int(robot[1])
        if y < 0 :
            print("The ROBOT is out of bound!")
            y = y + int(robot[1])
        else :
            print("The new position of ROBOT is: ", x, ",", y, "(x, y)")
    elif robot[0] == "D" :
        y = y + int(robot[1])
        if y >= matrix :
            print("The ROBOT is out of bound!")
            y = y - int(robot[1])
        else :
            print("The new position of ROBOT is: ", x, ",", y, "(x, y)")
    elif robot[0] == "L" :
        x = x - int(robot[1])
        if x < 0 :
            print("The ROBOT is out of bound!")
            x = x + int(robot[1])
        else :
            print("The new position of ROBOT is: ", x, ",", y, "(x, y)")
    elif robot[0] == "R" :
        x = x + int(robot[1])
        if x >= matrix :
            print("The ROBOT is out of bound!")
            x = x - int(robot[1])
        else :
            print("The new position of ROBOT is: ", x, ",", y, "(x, y)")
    elif robot[0] == "EXIT" :
        break